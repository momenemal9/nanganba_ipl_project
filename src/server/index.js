//requiring the default file system module
const fs = require('fs');
//requiring the npm library for csv to json conversion
const csvToJson = require('csvtojson');
const generateJsonFiles = require('./generateJsonFiles');
const ipl = require('./ipl');

//Initializing path for the csv files
const matchesCsvPath = '../data/matches.csv';
const deliveriesCsvPath = '../data/deliveries.csv';


//Obtaining json data from matches.csv file
csvToJson()
  .fromFile(matchesCsvPath)
  .then(matches=> {

        //problem 1 - calculating the matches played per year
        const matchesPlayedPerYear = ipl.matchesPlayedPerYear(matches);
        generateJsonFiles(matchesPlayedPerYear, '../public/output/matchesPlayedPerYear.json');


        //problem 2 - calculating the matches won by per team per year
        const matchesWonPerTeamPerYear = ipl.matchesWonPerTeamPerYear(matches);
        generateJsonFiles(matchesWonPerTeamPerYear, '../public/output/matchesWonPerTeamPerYear.json');

        //obtaining json data from deliveries.csv file
        csvToJson()
        .fromFile(deliveriesCsvPath)
        .then(deliveries => {

                //problem 3 - calculating the extra runs conceded per team at a given season
                const extraRunsPerTeam = ipl.extraRunsPerTeam(matches, deliveries, 2016);
                generateJsonFiles(extraRunsPerTeam, '../public/output/extraRunsPerTeam.json');


                //problem 4 - calculating the top 10 economic bowlers at a given season
                const top10EconomicalBowlers = ipl.top10EconomicalBowlers(matches, deliveries, 2015);
                generateJsonFiles(top10EconomicalBowlers, '../public/output/top10EconomicalBowlers.json');

        });
  });



